library ieee;
use ieee.std_logic_1164.all, ieee.std_logic_arith.all;

entity registers is
  port(RR1,RR2,WR:in std_logic_vector(4 downto 0);
       WD:in std_logic_vector(31 downto 0);
       RegWrite,ck:in std_logic;
       RD1,RD2:out std_logic_vector(31 downto 0));
end registers;

architecture beh of registers is
type COOL_REG is array(0 to 31) of std_logic_vector(31 downto 0);
signal regContents : COOL_REG;
begin
process(ck)
variable a,b,c: integer;
variable flag : boolean := FALSE;
begin

if flag = FALSE then
  regContents(0) <= (others => '0');
  --regContents(8) <= (others => '0');
  regContents(16) <= "00000000000000000000000000000100";
  regContents(17) <= "00000000000000000000000000010000";
  regContents(23) <= "00000000000000000000000000001111";
 -- regContents(23) <= "00000000000000000000000000000011";
  flag := TRUE;
end if;

a := conv_integer(unsigned(RR1));
b := conv_integer(unsigned(RR2));
c := conv_integer(unsigned(WR));

if ck = '0' then
  RD1 <= regContents(a);
  RD2 <= regContents(b);
elsif ck = '1' and RegWrite = '1' and c /= 0 then
  regContents(c) <= WD;
end if;

end process;
end beh;