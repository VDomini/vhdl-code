library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CPU is
  port(clk:in std_logic; Overflow:out std_logic);
end CPU;

architecture struc of CPU is


  component ALU32 is
  generic( n: natural:= 32);
  port( a,b: in std_logic_vector(n-1 downto 0);
        Oper: in std_logic_vector(3 downto 0);
        Result:buffer std_logic_vector(n-1 downto 0);
        Zero:buffer std_logic;
        Overflow:out std_logic); 
  end component;
  
  component ALUControl is
  port(ALUOp : in std_logic_vector(1 downto 0);
     Funct : in std_logic_vector(5 downto 0);
     Operation : out std_logic_vector(3 downto 0));
  end component;

  component AND2 is
  port(x,y:in std_logic;z:out std_logic);
  end component;

  component Control is
  port(Opcode : in std_logic_vector(5 downto 0);
     RegDst, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite, Jump : out std_logic;
     ALUOp : out std_logic_vector(1 downto 0));
  end component;
  
  component DataMemory is
  port(WriteData:in std_logic_vector(31 downto 0);
    Address:in std_logic_vector(31 downto 0);
    MemRead,MemWrite,ck:in std_logic;
    ReadData:out std_logic_vector(31 downto 0));
  end component;
  
  component InstMemory is
  port(Address:in std_logic_vector(31 downto 0);  
      ck : in std_logic;
     ReadData:out std_logic_vector(31 downto 0));
  end component;
  
  component MUX5 is
  port(x,y:in std_logic_vector (4 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(4 downto 0));
  end component;
  
  component MUX32 is
  port(x,y:in std_logic_vector (31 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(31 downto 0));
  end component;
  
  component registers is
  port(RR1,RR2,WR:in std_logic_vector(4 downto 0);
       WD:in std_logic_vector(31 downto 0);
       RegWrite,ck:in std_logic;
       RD1,RD2:out std_logic_vector(31 downto 0));
  end component;
  
  component ShiftLeft2Jump is
  port(x:in std_logic_vector(25 downto 0);
     y:in std_logic_vector(3 downto 0);
    z:out std_logic_vector(31 downto 0));
  end component;
  
  component ShiftLeft2 is
  port(x:in std_logic_vector(31 downto 0);
       y:out std_logic_vector(31 downto 0));
  end component;
  
  component SignExtend is
  port(x:in std_logic_vector(15 downto 0);
       y:out std_logic_vector(31 downto 0));
  end component;
  
  component PC is
  port(clk:in std_logic;
     AddressIn:in std_logic_vector(31 downto 0);
     AddressOut:out std_logic_vector(31 downto 0));
  end component;

-- Various signals
  signal A,C,D,E,F,G,H,J,K,L,M,N,P,Q:STD_LOGIC_VECTOR(31 downto 0);
  signal B:STD_LOGIC_VECTOR(4 downto 0);
  signal R:STD_LOGIC;

  -- Full 32-bit instruction
  signal Instruction:STD_LOGIC_VECTOR(31 downto 0);

  -- Out of ALU control
  signal Operation:STD_LOGIC_VECTOR(3 downto 0);

  -- Control out
  signal RegDst,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite,Jump,Zero:STD_LOGIC;
  signal ALUop:STD_LOGIC_VECTOR(1 downto 0);
  signal four:STD_LOGIC_VECTOR(31 downto 0):="00000000000000000000000000000100";
  
begin
  
  -- IF --- rod
  
  --IN: CLK, four, M , R, Q, Jump
  --OUT: Instruction, L
  PC32:PC port map(CLK,P,A);
  IM:InstMemory port map(A,clk,Instruction);
  ADD4:ALU32 port map(A,four,"0010",L,open,open);
  MuxPC_0:MUX32 port map(L,M,R,N);
  MuxPC_1:MUX32 port map(N,Q,Jump,P);
    
  --IF - Qandeel
  clk : in std_logic;

        pc_src : in std_logic;
        pc_branch_jump : in std_logic_vector(31 downto 0);

        pc_plus_four, instruction : out std_logic_vector(31 downto 0)
    );

  --IN: CLK, four, pc_src, pc_jump_branch
  --OUT: Instruction, four
  PC32:PC port map(CLK,pc_next, pc_now);
  IM:InstMemory port map(A,clk,Instruction);
  ADD4:ALU32 port map(A,four,"0010",L,open,open);
  MuxPC_0:MUX32 port map(L,M,R,N);
  MuxPC_1:MUX32 port map(N,Q,Jump,P);
    
  signal curr_pc, next_pc, pc_plus_four_buff, instruction_buff : std_logic_vector(31 downto 0);
    signal four : std_logic_vector(31 downto 0) := (2 => '1', others => '0');
    signal add_oper : std_logic_vector(3 downto 0) :=  "0010"  
    
    
  --ID -- ROD
  
  --IN: Instruction, BFinal, J, L(buffer), RegWriteFinal
  --OUT: RegDst,Branch,MemRead,MemToReg,MemWrite,ALUSrc,Jump,ALUop,C,D,E
  Control_0:Control port map(Instruction(31 downto 26),RegDst,Branch,MemRead,MemToReg,MemWrite,ALUSrc,RegWrite,Jump,ALUop);
  Reg:registers port map(Instruction(25 downto 21),Instruction(20 downto 16),B,J,RegWrite,clk,C,D);
  SignExt:SignExtend port map(Instruction(15 downto 0),E);
  

--ID -- QANDEEL
  entity id_stage is
    port(
        clk : in std_logic;
       
        instruction, pc_plus_four_id, write_data : in std_logic_vector(31 downto 0);
        write_addr : in std_logic_vector(4 downto 0);
        regwrite_wb : in std_logic;

        reg1_data, reg2_data, se_data, pc_plus_four_ex : out std_logic_vector(31 downto 0);
        write_addr1, write_addr2 : out std_logic_vector(4 downto 0);
        aluop : out std_logic_vector(1 downto 0);
        regwrite, memtoreg, branch, memread, memwrite, regdest, alusrc : out std_logic
        );
  --IN: Instruction, BFinal, J, L(buffer), RegWriteFinal
  --OUT: RegDst,Branch,MemRead,MemToReg,MemWrite,ALUSrc,Jump,ALUop,C,D,E
  Control_0:Control port map(Instruction(31 downto 26),RegDst,Branch,MemRead,MemToReg,MemWrite,ALUSrc,RegWrite,Jump,ALUop);
  Reg:registers port map(Instruction(25 downto 21),Instruction(20 downto 16),B,J,RegWrite,clk,C,D);
  SignExt:SignExtend port map(Instruction(15 downto 0),E);
  
  ctrl : control port map(instruction(31 downto 26), regdest_buff, branch_buff, memread_buff, memtoreg_buff, memwrite_buff, alusrc_buff, regwrite_buff, open, aluop_buff);

    reg : registers port map(instruction(25 downto 21), instruction(20 downto 16), write_addr, write_data, regwrite_wb, clk, reg1_data_buff, reg2_data_buff);

    se : sign_extend port map(instruction(15 downto 0), se_data_buff);


  
  --EX
  
  --IN: Buffers: Branch,MemRead,MemToReg,MemWrite,Jump
  --    RegDst,ALUSrc,ALUop,C,D,E,L
  --OUT: Zero, M, B, G, Q
  ALUControl_0:ALUControl port map(ALUop,Instruction(5 downto 0),Operation);
  Mux5_0:Mux5 port map(Instruction(20 downto 16),Instruction(15 downto 11),RegDst,B);
  Mux32_0:Mux32 port map(D,E,ALUSrc,F);
  ALU_0:ALU32 port map(C,F,Operation,G,Zero,open); 
  SLLALU:ShiftLeft2 port map(E,K);
  ADD:ALU32 port map(L,K,"0010",M,open,open);
  SLLJ:ShiftLeft2Jump port map(Instruction(25 downto 0),L(31 downto 28),Q);
    
    
  --MEM
  
  --IN: D, G, L, M, Q, CLK, Zero, Branch,MemRead,MemWrite,Jump
  --    Buffers: MemtoReg, RegWrite
  --OUT: H, R
  DM:DataMemory port map(D,G,MemRead,MemWrite,CLK,H); 
  AND_0:AND2 port map(Branch,Zero,R);
  
    
  -- WB  
  
  --IN: G, H, B, MemtoReg, RegWrite
  --Out: J, RegWriteFinal, Bfinal
  Mux32_1:Mux32 port map(G,H,MemtoReg,J);  
  
  
  
end struc;