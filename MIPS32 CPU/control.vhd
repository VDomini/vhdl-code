
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Control is
port(Opcode : in std_logic_vector(5 downto 0);
     RegDst, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite, Jump : out std_logic;
     ALUOp : out std_logic_vector(1 downto 0));
end Control;

architecture Behavioral of Control is
begin
  process(Opcode)
    variable temp : std_logic_vector(9 downto 0);
     begin
                
       case Opcode is
        when "000000" =>
          temp := "0100100010";
        when "100011" =>
          temp := "0011110000";
        when "101011" =>
          temp := "0U1U001000";
        when "000100" =>
          temp := "0U0U000101";
        when "000010" =>
          temp := "1UUU0000UU";
        when others =>
          null;
        end case;
        
        Jump <= temp(9);
        RegDst <= temp(8);
        ALUSrc <= temp(7);
        MemtoReg <= temp(6);
        RegWrite <= temp(5);
        MemRead <= temp(4);
        MemWrite <= temp(3);
        Branch <= temp(2);
        ALUOp(1) <= temp(1);
        ALUOp(0) <= temp(0);
        
      end process;
    end Behavioral;
