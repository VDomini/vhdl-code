library ieee;
use ieee.std_logic_1164.all, ieee.std_logic_arith.all;

entity DataMemory is
port(WriteData:in std_logic_vector(31 downto 0);
     Address:in std_logic_vector(31 downto 0);
     MemRead,MemWrite, ck:in std_logic;
     ReadData:out std_logic_vector(31 downto 0));
end DataMemory;

architecture beh of DataMemory is
type memoryArray is array(0 to 2**8-1) of std_logic_vector(7 downto 0);
signal memoryContents:memoryArray;
begin
process(ck)
variable j : integer;
variable flag : boolean := FALSE;
begin

if flag = FALSE then
  
  memoryContents(4) <= "00000000";
  memoryContents(5) <= "00000000";
  memoryContents(6) <= "00000000";
  memoryContents(7) <= "00001001";
  
  memoryContents(8) <= "00000000";
  memoryContents(9) <= "00000000";
  memoryContents(10) <= "00000000";
  memoryContents(11) <= "00001000";
  
  flag := TRUE;
  
end if;

j := conv_integer(unsigned(Address));
if ck = '0' and MemRead = '1' and Address /= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU" then
 ReadData <= memoryContents(j) & memoryContents(j+1) 
              & memoryContents(j+2) & memoryContents(j+3);
end if;
 
if ck = '1' and MemWrite = '1' and Address /= "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU" then
memoryContents(j)<=Writedata(31 downto 24);
memoryContents(j+1)<=Writedata(23 downto 16);
memoryContents(j+2)<=Writedata(15 downto 8);
memoryContents(j+3)<=Writedata(7 downto 0);
end if;

end process;
end beh;
