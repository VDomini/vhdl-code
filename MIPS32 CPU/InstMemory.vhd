library ieee;
use ieee.std_logic_1164.all; use ieee.std_logic_arith.all;

entity InstMemory is
port(Address:in std_logic_vector(31 downto 0);  
      ck : in std_logic;
     ReadData:out std_logic_vector(31 downto 0));
end InstMemory;

architecture beh of InstMemory is
type memoryArray is array(0 to 255) of std_logic_vector(7 downto 0);
signal memContents:memoryArray;
begin
process(Address)
variable i: integer;
variable first: boolean:= true;
begin
  
--  if(first) then
--  -- lw $t0, 0($s0) Good
--  memcontents(0)<="10001110";
--  memcontents(1)<="00001000";
--  memcontents(2)<="00000000";
--  memcontents(3)<="00000000";
--    -- lw $t1,4($s0) Good
--  memcontents(4)<="10001110";
--  memcontents(5)<="00001001";
--  memcontents(6)<="00000000";
--  memcontents(7)<="00000100";
--  -- beq $t0,$t1,L Good
--  memcontents(8)<="00010001";
--  memcontents(9)<="00001001";
--  memcontents(10)<="00000000";
--  memcontents(11)<="00001000";
--  -- sub $t2,$s1,$s7 Good
--  memcontents(12)<="00000010";
--  memcontents(13)<="00110111";
--  memcontents(14)<="01010000";
--  memcontents(15)<="00100010";
--  -- j exit
--  memcontents(16)<="00001000";
--  memcontents(17)<="00000000";
--  memcontents(18)<="00000000";
--  memcontents(19)<="00000110";
--  -- L:  add $t2,$s1,$s7 Good
--  memcontents(20)<="00000010";
--  memcontents(21)<="00110111";
--  memcontents(22)<="01010000";
--  memcontents(23)<="00100000";
--  --exit: sw $t2,8($s0) Good
--  memcontents(24)<="10101110";
--  memcontents(25)<="00001010";
--  memcontents(26)<="00000000";
--  memcontents(27)<="00001000";
--
--first:= false;

if(first) then
-- lw $t0, 0($s0) Good
  memcontents(0)<="10001110";
  memcontents(1)<="00001011";
  memcontents(2)<="00000000";
  memcontents(3)<="00000000";
  -- lw $t0, 0($s0) Good
  memcontents(4)<="10001110";
  memcontents(5)<="00001000";
  memcontents(6)<="00000000";
  memcontents(7)<="00000000";
    -- lw $t1,4($s0) Good
  memcontents(8)<="10001110";
  memcontents(9)<="00001001";
  memcontents(10)<="00000000";
  memcontents(11)<="00000100";
  -- beq $t0,$t1,L Good
  memcontents(12)<="00010001";
  memcontents(13)<="00001001";
  memcontents(14)<="00000000";
  memcontents(15)<="00000010";
  -- sub $t2,$s1,$s7 Good
  memcontents(16)<="00000010";
  memcontents(17)<="00110111";
  memcontents(18)<="01010000";
  memcontents(19)<="00100010";
  -- j exit
  memcontents(20)<="00001000";
  memcontents(21)<="00000000";
  memcontents(22)<="00000000";
  memcontents(23)<="00000111";
  -- L:  add $t2,$s1,$s7 Good
  memcontents(24)<="00000010";
  memcontents(25)<="00110111";
  memcontents(26)<="01010000";
  memcontents(27)<="00100000";
  --exit: sw $t2,8($s0) Good
  memcontents(28)<="10101110";
  memcontents(29)<="00001010";
  memcontents(30)<="00000000";
  memcontents(31)<="00001000";

first:= false;
end if;
i := conv_integer(unsigned(Address));

ReadData <= memContents(i)(7 downto 0)&memContents(i+1)(7 downto 0)
          &memContents(i+2)(7 downto 0)&memContents(i+3)( 7 downto 0);
end process;

end beh;